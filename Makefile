CC=pdflatex
ODIR=output
CFLAGS=-output-directory=$(ODIR) -interaction=nonstopmode
TARGET=src/annual-calendar.tex
PDF=output/annual-calendar.pdf

makelatex:
	$(CC) $(CFLAGS) $(TARGET)

clean:
	rm -f $(ODIR)/{*.aux,*.log,*.pdf}

show:
	evince $(PDF) &
